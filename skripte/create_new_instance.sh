#!/bin/bash

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root or use sudo."
  exit 1
fi

# Function to list Docker ports in use that start with 8
list_docker_ports_starting_with_8() {
    if ! command -v docker &> /dev/null; then
        echo "Docker is not installed or not in the PATH"
        exit 1
    fi

    docker_ports=$(docker ps --format '{{.Ports}}' | grep -o '[0-9]*->' | grep -o '^8[0-9]*' | sort -n | uniq)
    if [[ -z "$docker_ports" ]]; then
        echo "No Docker ports starting with 8 are currently in use."
    else
        echo "$docker_ports"
    fi
}

is_port_in_use() {
    local port="$1"
    local ports_in_use="$2"
    if [[ "$ports_in_use" =~ (^|[[:space:]])"$port"($|[[:space:]]) ]]; then
        return 0  # Port is in use
    else
        return 1  # Port is not in use
    fi
}

# Prompt the user for input
read -p "Enter the domain name: " domain_name


# Validate domain name input (basic validation)
if [[ -z "$domain_name" ]]; then
    echo "Domain name cannot be empty"
    exit 1
fi

SERVER_IP=$(curl -s http://checkip.amazonaws.com)
DOMAIN_IP=$(dig +short $domain_name | grep -E '^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$')
if [ -z "$DOMAIN_IP" ]; then
  echo "Failed to resolve IP address for domain: $domain_name"
  exit 1
fi
if [ "$SERVER_IP" == "$DOMAIN_IP" ]; then
  echo "The domain $domain_name is pointing to the public IP address of this server ($SERVER_IP)."
else
  echo "The domain $domain_name is NOT pointing to the public IP address of this server."
  echo "Server IP: $SERVER_IP"
  echo "Domain IP: $DOMAIN_IP"
  exit 1
fi

# List Docker ports in use that start with 8
echo "Docker ports currently in use (starting with 8):"
list_docker_ports_starting_with_8

# Prompt for port
docker_ports_in_use=$(list_docker_ports_starting_with_8)
while true; do
    read -p "Enter the port (must start with 8 and not be in use): " port_app
    # Validate port input
    if ! [[ "$port_app" =~ ^8[0-9]+$ ]]; then
        echo "Port must start with 8 and be a number"
        continue
    fi
    if is_port_in_use "$port_app" "$docker_ports_in_use"; then
        echo "Port $port_app is already in use. Please choose another port."
        continue
    fi
    break
done

read -p "Enter the version: " version

# Validate version input (basic validation)
if [[ -z "$version" ]]; then
    echo "Version cannot be empty"
    exit 1
fi

# Copy the 'template' folder to a new folder with the same name as the domain name
if [[ -d "template" ]]; then
    cp -r template "$domain_name"
    echo "Template folder copied to $domain_name"
else
    echo "Template folder does not exist"
    exit 1
fi

# File manipulation based on the version number
if (( version >= 16 )); then
    if [[ -f "$domain_name/config/nginx_odoo12.conf" ]]; then
        rm "$domain_name/config/nginx_odoo12.conf"
#        echo "Deleted $domain_name/config/nginx_odoo12.conf"
    fi
    if [[ -f "$domain_name/config/nginx_odoo16.conf" ]]; then
        mv "$domain_name/config/nginx_odoo16.conf" "$domain_name/config/nginx.conf"
#        echo "Renamed $domain_name/config/nginx_odoo16.conf to nginx.conf"
    fi
else
    if [[ -f "$domain_name/config/nginx_odoo12.conf" ]]; then
        mv "$domain_name/config/nginx_odoo12.conf" "$domain_name/config/nginx.conf"
#        echo "Renamed $domain_name/config/nginx_odoo12.conf to nginx.conf"
    fi
    if [[ -f "$domain_name/config/nginx_odoo16.conf" ]]; then
        rm "$domain_name/config/nginx_odoo16.conf"
#        echo "Deleted $domain_name/config/nginx_odoo16.conf"
    fi
fi

# Specify the files to be processed
files=("$domain_name/config/nginx.conf" "$domain_name/env/.env" "$domain_name/Dockerfile")

port_lp="7${port_app:1}"
# Iterate over each file and replace placeholders with user input
for file in "${files[@]}"; do
    if [[ -f "$file" ]]; then        
        # Use | as the delimiter for sed to avoid issues with / in inputs
        sed -i "s|{DOMAIN}|$domain_name|g" "$file"
        sed -i "s|{port_app}|$port_app|g" "$file"
        sed -i "s|{VERSION}|$version|g" "$file"
        sed -i "s|{port_lp}|$port_lp|g" "$file"
        
        echo "Processed $file"
    else
        echo "File $file does not exist"
    fi
done

# Add symbolic link for nginx configuration
ln -fs "/home/$domain_name/config/nginx.conf" "/etc/nginx/sites-enabled/$domain_name.conf"

if nginx -t; then
    # Reload nginx if configuration is successful
    echo "NGINX test ok."
else
    # Remove symbolic link if configuration fails
    rm "/etc/nginx/sites-enabled/$domain_name.conf"
    echo "NGINX configuration test failed. Symbolic link removed."
    exit 1
fi
# Run Certbot for obtaining SSL certificate
certbot --nginx -d $domain_name

certbot_exit_status=$?
if [ $certbot_exit_status -eq 0 ]; then
    echo "Certbot successfully obtained SSL certificate."
else
    echo "Certbot failed to obtain SSL certificate. Exiting."
    exit 1
fi
# Verify nginx configuration
if nginx -t; then
    # Reload nginx if configuration is successful
    systemctl reload nginx
    echo "NGINX reloaded."
else
    # Remove symbolic link if configuration fails
    rm "/etc/nginx/sites-enabled/$domain_name.conf"
    echo "NGINX configuration test failed. Symbolic link removed."
fi

chmod +x "./$domain_name/prod"
chmod -R 777 "./$domain_name/data/"
cd $domain_name
bash prod up -d

echo "Done"